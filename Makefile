# This Makefile only exists for my own purposes, and isn't actually packaged
# or used to install hex on target machines.

all:
	python setup.py build

clean:
	python setup.py clean

install:
	python setup.py install
	# Because install can do bad things, fix it.
	rm hexlib/*.pyc

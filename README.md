HEX IRC Bot System: Version 0.4.10
---------------------------------

"Hex_" (yes, this might have been a reference to Terry Pratchett's magical supercomputer)
is an IRC bot that I started writing three or four years ago for the handful
of IRC channels I administer.

Development of this bot was rather haphazard over the years, because it only
lived on my server where it ran and not anywhere else. That meant I tended
to edit with nano whenever I wanted to add a "feature", and added features
just by adding new lines to the main script.

So, the code was very ugly, terrible, awful, and bloated. Really-- I was embarrrased.

However, hex also was one of my main projects over the years. It spun off
several other projects; the [xkcd Python library](http://github.com/TC01/python-xkcd), among others.

So I finally decided to throw it in (private, at first) version control and improve
things, adding a plugin system, adding new features, and just generally improving
the bot until it became something anyone could download. I'm pleased to say we've
met that requirement, more or less: with just a config file, you too can have an IRC
bot running on your server.

Hex is still in heavy development / alpha, I would say. While it is now available
for download from PyPI, this is primarily so that certain people I know (see "Notable
end users / testers") can deploy the bot for their own purpose. Use at your peril,
don't expect everything to make sense.

Eventually, it should become possible for other people to submit plugins with
commands and stuff (once I implement a plugin system for commands instead of
what we have now).

## Installing

You can install the latest development version of Hex as follows:

```
git clone https://bitbucket.org/TC01/hex/
cd hex/
python setup.py sdist --formats=zip
sudo pip install dist/hexlib*
```

## Deployment

To run hex, simply run:

```
hex [config]
```

Where [config] is the name of a configuration file stored in /etc/hex.

If your config file is located elsewhere, instead run:

```
hex -p $path [config]
```

Yes, this is a bit silly, and may get fixed later.

## Running hexd

If you are on a Linux system with ```tmux``` and ```sudo```, you can make use
of the included ```hexd``` shellscript. hexd is based on a script to start
and stop a Minecraft server, but I've modified it to be more useful for this
project.

Example:

```
$ hexd start config.conf
$ hexd console config.conf
$ hexd restart config.conf
```

This script can run multiple instances of hex in multiple tmux sessions, as
shown above. Note that if a config is not provided, hexd defaults to "hex.conf".

hexd tries to run as the user 'irc'.

## Example Configuration:

Look in "configs/" for some of the config files that I'm using hex with.

## Notable Features

### Listen/commands server

If enabled in the configuration (it is *disabled* by default) with the
```listen_server``` option, hex can start a simple TCP server running on
port 5025 (by default). This allows the bot to accept external commands
or input from another service.

For instance, a sample plugin is included that listens for lines of the form
"#channel message" and passes the message ```message``` to the channel
```#channel```, if such a line is sent to tcp/5025.

There are, of course, potential security issues here. We strongly recommend
that you **DO NOT** enable this feature unless you are running hex behind
a firewall. Ideally, if running such a server, you would deploy hex on
a dedicated virtual machine firewalled appropriately. Note, though, that
unless plugins are enabled, nothing happens by default with messages sent
to the listen server.

While making this more secure is something that would be nice, it's not
really a priority for me at the moment. Patches are welcome if you strongly
disagree.

## CREDITS:

### Developers:

* Ben Rosser <rosser.bjr@gmail.com>

### Notable end users / testers:

* Galen Schretlen
* Bryce Miller
* Greg Sowiak

### Used code from:

* Cameron Higby-Naquin: [mtglib](https://github.com/chigby/mtg)
* StackOverflow user "smackshow": names extension to IRCClient
(https://stackoverflow.com/questions/6671620/list-users-in-irc-channel-using-twisted-python-irc-framework)

And of course, this would all be impossible without Python and Twisted.

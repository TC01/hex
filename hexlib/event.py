# The base HexEvent class, used to schedule events.

import datetime

# Schedule "ENUM"
NO_SCHEDULE = -1
REPEAT_DAILY = 0
REPEAT_YEARLY = 1

class HexEvent:
	
	def __init__(self, client, channel, date, message, silent=False):
		self.client = client
		self.channel = channel
		self.silent = silent
		self.message = message
		self.date = date
		
		# Repeating flags.
		self.repeating = False
		self.schedule = NO_SCHEDULE
		
	def getStartTime(self):
		"""Returns the start time, in seconds, from the scheduled event time."""
		startDate = self.date - datetime.datetime.now()
		return startDate.total_seconds()
		
	def run(self):
		"""Called by the twisted scheduler, actually runs the event. Intended
		to be overriden."""
		print "[event] " + str(self.date) + ": " + self.message
		if not self.silent:
			self.client.logSay(self.channel, self.message)
			
		# Schedule the event to happen again.
		if self.repeating:
			if self.schedule == REPEAT_DAILY:
				self.date = self.date.replace(date.year, date.month, date.day + 1)
			elif self.schedule == REPEAT_YEARLY:
				self.date = self.date.replace(date.year + 1, date.month, date.day)				
			self.client.scheduleEvent(self)
			
	def setSchedule(self, schedule):
		"""Set the repeating schedule."""
		if self.schedule == -1:
			self.repeating = False
			self.schedule = NO_SCHEDULE
		else:
			self.repeating = True
			self.schedule = schedule
# Config parser for hexlib; loads *.conf from [dir]/hex/
# Will later be expanded to load other configs as well.

import ConfigParser
import os

class HexSettings:

	def __init__(self, config):
		# The "connection settings", we have a special getter for them
		self.name = config.get("irc", "name")
		self.channel = config.get("irc", "main_channel")
		self.server = config.get("irc", "server")
		self.port = self.parseInteger(config.get("irc", "port"), 6667)

		self.logging = self.parseBoolean(config.get("main", "logging"), False)
		self.logdir = os.path.abspath(config.get("main", "log_location"))
		self.otherChannels = self.parseArray(config.get("main", "other_channels"))
		self.noCommandChannels = self.parseArray(config.get("main", "no_commands_in"))
		self.noLoggingChannels = self.parseArray(config.get("main", "no_logging_in"))
		self.commandChar = config.get("main", "command_char")
		self.listenServer = self.parseBoolean(config.get("main", "listen_server"), False)
		
		# Plugin locations - prune for invalid locations.
		self.pluginLocations = self.parseArray(config.get("main", "plugin_locations"))
		for location in self.pluginLocations:
			if not os.path.exists(location):
				self.pluginLocations.remove(location)

		self.useNickserv = self.parseBoolean(config.get("nickserv", "auth_to_nickserv"), False)
		self.nickservEmail = config.get("nickserv", "email_address")
		self.password = config.get("nickserv", "password_file")

		self.useAllPlugins = self.parseBoolean(config.get("plugins", "use_all"), False)
		self.plugins = []

		for item in config.items("plugins"):
			name, value = item
			if name != "use_all":
				if value == "True":
					self.plugins.append(name)

	def getConnectionSettings(self):
		"""Return the settings required to establish a connection.
		   A tuple of (name, server, channel, port)."""
		return self.name, self.server, self.channel, self.port

	def parseInteger(self, setting, default):
		"""Helper function to try and parse setting string into an integer. Defaults to default on failure."""
		try:
			integer = int(setting)
		except:
			integer = default
		return integer

	def parseBoolean(self, setting, default):
		"""Boolean version of parseInteger."""
		if setting == "True":
			return True
		elif setting == "False":
			return False
		return default

	def parseArray(self, setting):
		"""Splits setting string by comma and removes spurious whitespace."""
		array = setting.split(",")
		newArray = []
		for string in array:
			newString = string.lstrip().rstrip()
			newString = newString.lstrip("\t").rstrip("\t")
			newArray.append(newString)

		# Fix case where string is empty
		if len(newArray) == 1 and newArray[0] == '':
			return []
		return newArray


def makeDefaultConfig(name, location, overwrite=False):
	"""Initialize a config file with name in location. If overwrite is set to True, remove existing file."""
	if not os.path.exists(location):
		return
	filename = os.path.join(location, name)
	if os.path.exists(filename) and not overwrite:
		return

	# Write main and irc sections of the config file
	config = ConfigParser.SafeConfigParser()

	config.add_section("irc")
	config.set("irc", "name", "Hex_")
	config.set("irc", "main_channel", "#gscc")
	config.set("irc", "server", "chat.freenode.net")
	config.set("irc", "port", "6667")

	config.add_section("main")
	config.set("main", "other_channels", "#gskk, #jhusps")
	config.set("main", "logging", "True")
	config.set("main", "log_location", "/srv/irc/gscc/")
	config.set("main", "no_commands_in", "")
	config.set("main", "no_logging_in", "")
	config.set("main", "command_char", "!")
	config.set("main", "listen_server", "False")
	config.set("main", "plugin_locations", "")

	config.add_section("nickserv")
	config.set("nickserv", "auth_to_nickserv", "True")
	config.set("nickserv", "password_file", "/etc/hex/hex_password.txt")
	config.set("nickserv", "email_address", "rosser.bjr@gmail.com")

	config.add_section("plugins")
	config.set("plugins", "use_all", "True")
	# The "[pluginname] = True/False" lines aren't needed if use_all is set
	config.set("plugins", "hex_isdown", "True")

	# Write actual file
	try:
		configfile = open(filename, 'wb')
		config.write(configfile)
		configfile.close()
		return True
	except:
		return False

def readConfig(name, location):
	"""Read a config file from location and return a HexSettings object."""
	if not os.path.exists(location):
		return
	filename = os.path.join(location, name)
	if not os.path.exists(filename):
		makeDefaultConfig(name, location)

	config = ConfigParser.RawConfigParser()
	config.read(filename)
	return HexSettings(config)

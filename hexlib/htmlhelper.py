#Some miscellaneous crap for dealing with URLs and links and so on

import htmlentitydefs as htmldefs
import re
import urllib2 as urllib

def htmlEntityDecode(s):
	"""Shamelessly stolen from the Python wiki, this thingy actually works"""
	return re.sub('&(%s);' % '|'.join(htmldefs.name2codepoint), 
		lambda m: unichr(htmldefs.name2codepoint[m.group(1)]), s)

def parseURLTitle(url):
	"""Extract the title of a URL"""
	try:
		url = unicode(url, "utf-8").encode("idna")
		raw = urllib.urlopen(url).read()
	except:
		return ""
	if not "<title>" in raw:
		return ""
		
	title = raw[raw.find('<title>') + len('<title>'):]
	title = title[:title.find('</title>')]
	title = title.replace('\n', '')
	title = title.lstrip()
	try:
		title = htmlEntityDecode(title)
		title.encode("utf-8")
	except:
		title = ""
	return title

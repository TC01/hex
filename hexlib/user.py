import datetime

class User:
	
	def __init__(self, name):
		self.name = name
		self.numMessages = 0
		self.timeJoined = datetime.datetime.now()

	def __str__(self):
		return "User object for user: " + self.name
		
	def __repr__(self):
		return "User object for user: " + self.name

	def getMessagesPerMinute(self):
		currentTime = datetime.datetime.now()
		deltaTime = currentTime - self.timeJoined
		numMinutes = deltaTime.total_seconds() / 60.0
		messagesPerMin = self.numMessages / numMinutes
		return messagesPerMin

	def rename(self, name):
		self.name = name

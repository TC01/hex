# Routines for authenticating to services, etc.

def getPassword(passwordFile):
	"""Helper routine to load the password from the password file."""
	file = open(passwordFile)
	password = file.read()
	file.close()
	password = password.rstrip()

	# We really should have some kind of encryption here or something.

	return password


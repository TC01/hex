#!/usr/bin/env python

# One day, we'll write a statistics backend for hex. 
# count_joins badly needs rewriting.
# The old logs on my system need to be ported to the new format...

import os
import sys

class Person:

	def __init__(self, name):
		self.joins = 1
		self.name = name

	def __str__(self):
		return self.name + ": " + str(self.joins)

	def __repr__(self):
		return self.__str__()

	def increment(self):
		self.joins += 1

def count_joins(logLocation):

	gscc_logs = os.path.join(logLocation, 'logs', 'gscc')
	gskk_logs = os.path.join(logLocation, 'logs', 'gskk')
	old_gscc_logs = os.path.join(logLocation, 'old_logs', 'GSCC')
	people = {}
	dates = []

	# The old logs first
	for path, dirs, logs in os.walk(old_gscc_logs):
		for logname in logs:
			date = logname[5:]
			log = open(os.path.join(path, logname), 'rb')
			for line in log:
				if "has joined" in line:
					name = line[:line.index(" has joined")]
					name = name.partition(" ")[2]
					if " " in name:
						continue
					if not name in people:
						person = Person(name)
						people.update({name:person})
					else:
						person = people[name]
						person.increment()
						people.update({name:person})
			log.close()

	# Now, starting from the end of the old, count new joins
	for path, dirs, logs in os.walk(gscc_logs):
		for logname in logs:
			date = logname[:-4]
			dates.append(date)
			filename = os.path.join(path, logname)
			log = open(filename)
			for line in log:
				if "has joined" in line:
					name = line[:line.index(" has joined")]
					name = name.partition(" ")[2]
					if " " in name:
						continue
					if not name in people:
						person = Person(name)
						people.update({name:person})
					else:
						person = people[name]
						person.increment()
						people.update({name:person})
			log.close()

	# Now, these are the logs from the exodus channel
	for path, dirs, logs in os.walk(gskk_logs):
		for logname in logs:
			date = logname[:-4]
			if not date in dates:
				log = open(os.path.join(path, logname))
				for line in log:
					if "has joined" in line:
						name = line[:line.index(" has joined")]
						name = name.partition(" ")[2]
						if " " in name:
							continue
						if not name in people:
							person = Person(name)
							people.update({name:person})
						else:
							person = people[name]
							person.increment()
							people.update({name:person})
				log.close()

	stats = people.values()
	stats.sort(key = lambda x: x.joins, reverse = True)
	return stats

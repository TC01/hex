from twisted.internet import protocol
from twisted.internet import reactor

from hexlib import client
from hexlib import plugins
from hexlib import server

class ControlFactory(protocol.Factory):
	
	"""
	A factory for a simple TCP-based "control" protocol, i.e.
	something that sends any strings received over to the IRC client,
	so plugins can make things happen when input is received.
	"""
	
	def __init__(self, settings, ircFactory):
		self.settings = settings
		self.protocol = server.ControlServer
		self.ircFactory = ircFactory

class IRCBotFactory(protocol.ClientFactory):

	# Define the class to connect to.
	protocol = client.IRCBot
	
	# Settings object.
	settings = None
	
	# Quitting flag.
	quitting = False
	
	# The connected protocol.
	connectedProtocol = None

	def __init__(self, channel, nickname):
		self.channel = channel
		self.nickname = nickname

	def configure(self, settings):
		self.settings = settings
		
		# Load the plugins that our settings told us to.
		useAllPlugins = False
		pluginsToLoad = []
		if self.settings != None:
			useAllPlugins = self.settings.useAllPlugins
			pluginsToLoad = self.settings.plugins
		self.plugins = plugins.loadPlugins(self.settings.pluginLocations, pluginsToLoad, useAllPlugins)

	def clientConnectionLost(self, connector, reason):
		if self.quitting:
			print "*** Lost connection, exiting."
			reactor.stop()
		else:
			print "*** Lost connection, reconnecting."
			connector.connect()

	def clientConnectionFailed(self, connector, reason):
		print "*** Could not connect: " + str(reason)
		reactor.stop()

	def buildProtocol(self, address):
		proto = protocol.ClientFactory.buildProtocol(self, address)
		self.connectedProtocol = proto
		return proto
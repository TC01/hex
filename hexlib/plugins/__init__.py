# hexlib.plugins - Plugin manager for Hex
# Plugins are:
#  -a Python file with functions = the functions in IRCClient.
#     -except self = calling client
#  A plugin calls itself

# Working procedure:
#   hexlib.plugins.loadPlugins() is called on startup
#   Registers all plugins to hexlib.plugins.plugins
#   hexlib.plugins.funcname() calls funcname() in all plugins
#   Uses try/except to catch bad ones.

import copy
import imp
import os
import sys

def loadPlugins(locations, toLoad, useAll=False):
	plugins = []

	# Set up the possible list of locations.
	locations.insert(0, os.path.join(getScriptLocation()))

	# Get a list of all possible plugin names
	names = []
	for location in locations:
		for path, dirs, files in os.walk(location):
			if path == location:
				for filename in files:
					if not (".pyc" in filename or "__init__" in filename):
						filename = filename[:-len(".py")]
						names.append(filename)
					
	# Now, use imp to load all the plugins we specified
	loadPath = copy.copy(locations)
	loadPath.extend(__path__)
	for name in names:
		if useAll or name in toLoad:
			print "*** Loading " + name
			try:
				test = sys.modules[name]
			except KeyError:
				fp, pathname, description = imp.find_module(name, loadPath)
				try:
					plugin = imp.load_module(name, fp, pathname, description)
					plugins.append(plugin)
				finally:
					if not fp is None:
						fp.close()

	return plugins

# Commands we support plugins for; they just call the right function
# in each plugin (I... think).
# Except condition: a plugin doesn't support the function, nothing
# to worry about.
# Maybe later we'll add some more sophisticated detection of this.

def privmsg(plugins, client, user, channel, msg, shortuser):
	for plugin in plugins:
		try:
			plugin.privmsg(client, user, channel, msg, shortuser)
		except AttributeError as error:
			# This means that the plugin doesn't override the privmsg method.
			# I hope.
			if not "privmsg" in str(error):
				print "Attribute error in plugin: ", sys.exc_info()[0]
				print error
		except Exception as baseError:
			print "Error in plugin: ", sys.exc_info()[0]
			print baseError

def externalMessage(plugins, client, message):
	for plugin in plugins:
		try:
			plugin.externalMessage(client, message)
		except AttributeError as error:
			# This means that the plugin doesn't override the method, I hope.
			if not "externalMessage" in str(error):
				print "Attribute error in plugin: ", sys.exc_info()[0]
				print error
		except Exception as baseError:
			print "Error in plugin: ", sys.exc_info()[0]
			print baseError

def getHelpText(plugins, client):
	text = ""
	for plugin in plugins:
		try:
			pluginText = plugin.getHelpText(client)
			text += pluginText + "\n"
		except AttributeError as error:
			# This means that the plugin doesn't override the method, I hope.
			if not "getHelpText" in str(error):
				print "Attribute error in plugin: ", sys.exc_info()[0]
				print error
		except Exception as baseError:
			print "Error in plugin: ", sys.exc_info()[0]
			print baseError		

# Helper functions
	
def getScriptLocation():
	"""Helper function to get the location of a Python file."""
	location = os.path.abspath("./")
	if __file__.rfind("/") != -1:
		location = __file__[:__file__.rfind("/")]
	return location

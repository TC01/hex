# hex_bash: support to spew random quotes from bash.org into the channel
# Probably should be replaced with a joint interface to qdb.us, but so it goes.

# I blame Galen for this feature. I think.

import bashquote

def privmsg(client, user, channel, msg, shortuser):
	if msg[0] != client.commandChar:
		return
	
	if "bash" == msg[1:]:
		quote = bashquote.getLatestQuote()
		client.logSay(channel, "bash.org quote " + str(bashquote.getLatestQuoteNum()) + ":")
		text = quote.getText().encode('ascii')
		lines = text.split('\n')
		for line in lines:
			client.logSay(channel, line)
	elif "bash rand" == msg[1:]:
		quote = bashquote.getRandomQuote()
		client.logSay(channel, "bash.org quote " + str(quote.getNumber()) + ":")
		text = quote.getText().encode('ascii')
		lines = text.split('\n')
		for line in lines:
			client.logSay(channel, line)
	elif (client.commandChar + "bash") in msg:
		try:
			number = int(msg[len("!bash ") + 1:])
			quote = bashquote.Quote(number)
			if not quote.isExists():
				client.logSay(channel, "Error: quote " + str(number) + " does not exist!")
			text = quote.getText().encode('ascii')
			lines = text.split('\n')
			for line in lines:
				client.logSay(channel, line)
		except:
			client.logSay(channel, "Error: invalid bash.org quote number.")

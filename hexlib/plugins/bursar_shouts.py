# bursar_shouts: Dragon shout module, allowing Dragon Shouts to be created.
# Basically, an alternate commands interface for sufficiently silly commands.

# Note to self: this code is really bad. It should probably be turned into stats.
# Yeah, definitely turned into stats.
# At the moment, only the FUS RO DOC dragon shout does anything.

import subprocess

import lodgeitlib

def privmsg(client, user, channel, msg, shortuser):
	if "shout" == msg[1:6]:
			client.logSay(channel, "+++ " + shortuser + " has shouted " + msg[7:] + " +++")
			if "shout FUS RO DOC" == msg[1:]:
					logText = "[bjr@mars gskk]$ grep 'Fus-Ro-Doc is now known as ' *\n"
					pipe = subprocess.Popen('grep "Fus-Ro-Doc is now known as" /srv/irc/gscc/logs/gskk/*', shell=True, stdout=subprocess.PIPE)
					output, error = pipe.communicate()
					logText += output
					pipe = subprocess.Popen('grep "Fus-Ro-Doc is now known as" /srv/irc/gscc/logs/gskk/*', shell=True, stdout=subprocess.PIPE)
					output, error = pipe.communicate()
					logText += "\n[bjr@mars GSCC]$ grep 'Fus-Ro-Doc is now known as ' *\n"
					logText += output
					pastebin = lodgeitlib.Lodgeit("http://bpaste.net/")
					pasteID = pastebin.new_paste(logText, None, private=True)
					pasteURL = pastebin.get_paste_by_id(pasteID).url
					client.logSay(channel, "Variations on FUS RO DOC: " + str(pasteURL))

# acmbot_listener: plugin that listens for data on the external port
# and echoes it into the channel if it's of the right form, namely:
#     CHANNEL message.

def externalMessage(client, message):
	channel, _, text = message.partition(' ')
	if channel in client.channels.keys():
		client.logSay(channel, text)
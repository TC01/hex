# bursar_monkey: Bursar's infamous ability to kick people who say "monkey", Librarian-style.
# Should *probably* be turned into a more generic kickword plugin, but meh.

def privmsg(client, user, channel, msg, shortuser):
	if "monkey" in msg.lower():
		if shortuser != client.nickname:
			if not "arosser.com" in user or "brosser.net" in user:
				client.logSay(channel, "Whoops!")
				client.logSay(channel, "Ook! Ook!")
				client.kick(channel, shortuser, reason="Ook! Ook? Ook. Ook!")

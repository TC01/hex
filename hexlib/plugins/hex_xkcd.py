# hex-xkcd: xkcd plugin for hex
# The real work is done in the xkcd library.

import xkcd

def privmsg(client, user, channel, msg, shortuser):
	if msg[0] != client.commandChar:
		return

	if "xkcd" == msg[1:].rstrip():
		comic = xkcd.getLatestComic()
		client.logSay(channel, comic.link + " " + comic.getAsciiTitle())
	elif "xkcd rand" == msg[1:].rstrip():
		comic = xkcd.getRandomComic()
		client.logSay(channel, comic.link + " " + comic.getAsciiTitle())
	elif "xkcd explain" == msg[1:].rstrip():
		comic = xkcd.getLatestComic()
		client.logSay(channel, comic.getExplanation() + " explanation for xkcd: " + comic.getAsciiTitle())
	elif (client.commandChar + "xkcd explain") in msg:
		try:
			number = int(msg[len("!xkcd explain "):])
			comic = xkcd.Comic(number)
			client.logSay(channel, comic.getExplanation() + " explanation for xkcd: " + comic.getAsciiTitle())
		except:
			client.logSay(channel, "Error: invalid xkcd number")
	elif (client.commandChar + "xkcd") in msg:
		try:
			number = int(msg[6:])
			comic = xkcd.Comic(number)
			client.logSay(channel, comic.link + " xkcd: " + comic.getAsciiTitle())
		except:
			client.logSay(channel, "Error: invalid xkcd number")

	# Also, support what ifs!
	if "whatif" == msg[1:]:
		article = xkcd.getLatestWhatIf()
		client.logSay(channel, article.link + " " + article.getTitle())
	elif "whatif rand" == msg[1:]:
		article = xkcd.getRandomWhatIf()
		client.logSay(channel, article.link + " " + article.getTitle())

def getHelpText(client):
	text = "xkcd plugin: get xkcd links automatically from IRC. Usage:\n"
	text += "!xkcd: get link to latest comic.\n"
	text += "!xkcd explain: get explanxkcd link for latest comic.\n"
	text += "!xkcd rand: get link to random comic.\n"
	text += "!xkcd [number]: get link to numbered comic.\n"
	text += "!xkcd explain [number]: get explainxkcd link for numbered comic.\n"
	return text

# hex_night: automating social interactions like saying "good night" to people.

from hexlib import stats

def privmsg(client, user, channel, msg, shortuser):
	if msg[0] != client.commandChar:
		return

	# Night command, to ping people.
	if client.commandChar + "morning" == msg:
		nightString = shortuser + " is awake again and wanted to notify everyone, "
		for nightuser in client.channels[channel].users:
			username = nightuser.name
			if username != client.nickname and username != shortuser:
				nightString += username + ", "
		nightString = nightString[:-2] + "."
		client.logSay(channel, nightString)

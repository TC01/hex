# hex_isdown: isup.me script to check if a site is up or down.

import urllib2 as urllib

def privmsg(client, user, channel, msg, shortuser):
	if msg[0] != client.commandChar:
		return
	
	if "isup" in msg or "isdown" in msg:
		site = msg[len("isup") + 2:]
		if "isdown" in msg:
			site = msg[len("isdown") + 2:]

		# Parse the site, and pass to isup.me
		if site.find("://") != -1:
			site[site.find("://") + len("://"):].rstrip("/")
		isupSite = urllib.urlopen("http://isup.me/" + site)
		isup = isupSite.read()
		isupSite.close()
		
		# Do some HTML scraping to find the status, then say it
		isup = isup[isup.find('<div id="container">'):]
		if "It's just you" in isup:
			client.logSay(channel, "It's just you. " + site + " is up.")
		else:
			client.logSay(channel, "It's not just you! " + site + " + looks down from here.")

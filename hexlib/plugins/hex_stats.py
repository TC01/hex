# hex-stats: stats plugin for the hexlib stats backend
# At the moment, the stats backend is terrible. My specific implementation
# stuff is hardcoded. Thus... for the love of everything, do NOT enable this plugin.

# One day, it'll be cool, I swear.

from hexlib import stats

def privmsg(client, user, channel, msg, shortuser):
	if msg[0] != client.commandChar:
		return
	
	#Joins command, to get a list of nicknames via private pastebin
	if (client.commandChar + "joins") in msg and self.channels[channel].hasOperator(shortuser):
		joinlist = stats.count_joins(self.logLocation)
		buffer = open("temp.out", 'wb')
		for person in joinlist:
			buffer.write(str(person) + "\n")
		buffer.close()
		buffer = open("temp.out")
		pastebin = lodgeitlib.Lodgeit("http://bpaste.net/")
		pasteID = pastebin.new_paste(unicode(buffer.read()), None, private=True)
		pasteURL = pastebin.get_paste_by_id(pasteID).url
		self.msg(shortuser, "Statistics module: list of all joins, sorted by frequency: " + str(pasteURL))				
		buffer.close()
		os.remove("temp.out")	

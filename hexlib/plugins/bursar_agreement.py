# bursar_agreement: randomly agree with people, using a "^".

import random

def privmsg(client, user, channel, msg, shortuser):
	prob = random.randint(0, 100) / 100.0
	if prob <= 0.022:
			client.logSay(channel, "^")

# bursar_memes: generic memes (xkcd, Linux, ...?) implemented in Bursar.

def privmsg(client, user, channel, msg, shortuser):
	if msg == "import antigravity":
			client.logSay(channel, "http://www.xkcd.com/353/")
	if msg == "import soul":
			client.logSay(channel, "http://www.xkcd.com/413/")
	if msg == "import skynet":
			client.logSay(channel, "http://www.xkcd.com/521/")
			client.kick(channel, shortuser, reason="Skynet Enabled")
	if msg == "make me a sandwich":
			client.logSay(channel, "What? Do it yourself.")
	if msg == "sudo make me a sandwich":
			client.logSay(channel, "Fine. http://xkcd.com/149/")

	#mv /dev/null command (linux joke)
	if (msg[:2] == "mv" and '/dev/null' in msg):
			if shortuser in client.operators:
					char = ''
					count = 3
					name = ""
					while msg[count+1] != '/':
							char = msg[count]
							name += char
							count += 1
					client.kick(channel, name, reason="Moved to /dev/null/")
			else:
					client.logSay(channel, "irc: mv: /dev/null: Permission denied")

# hex_links: load HTTP links and output the title of the page to the channel.
# Uses a helper library in hexlib.

from hexlib import htmlhelper

def privmsg(client, user, channel, msg, shortuser):
	if "http:" in msg or "https:" in msg:
		if "http:" in msg:
			link = msg[msg.find("http:"):]
		else:
			link = msg[msg.find("https:"):]
		if link.find(" ") != -1:
			link = link[:link.find(" ")]
		linkTitle = htmlhelper.parseURLTitle(link)
		if linkTitle != "":
			# Twisted doesn't do unicode, so sometimes this will fail.
			try:
				linkTitle = str(linkTitle).encode('ascii')
				client.logSay(channel, linkTitle)
			except:
				pass
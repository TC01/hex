from hexlib.user import User

class Channel:
	
	def __init__(self, name):
		self.name = name
		self.users = []
		self.operators = []
	
	def __str__(self):
		return "Channel object for channel: " + self.name
		
	def __repr__(self):
		return "Channel object for channel: " + self.name
	
	def addUser(self, username):
		if not self.hasUser(username):
			self.users.append(User(username))
		return self.getUser(username)
		
	def addOperator(self, username):
		if not self.hasOperator(username):
			self.operators.append(username)
		
	def removeUser(self, username):
		if self.hasUser(username):
			oldUser = self.getUser(username)
			self.users.remove(oldUser)
			self.removeOperator(oldUser)
			
	def removeOperator(self, username):
		if self.hasOperator(username):
			self.operators.remove(username)
			
	def hasUser(self, username):
		for user in self.users:
			if user.name == username:
				return True
		return False
		
	def hasOperator(self, username):
		user = self.getUser(username)
		if user != None and username in self.operators:
			return True
		return False
		
	def getUser(self, username):
		"""??? I think the point here was that we could track TC01 == TC01_
		But this function is pointless. Unless I figure out why I wrote it."""
		for user in self.users:
			if user.name == username:
				return user
		return None

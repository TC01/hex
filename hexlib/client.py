import datetime
import hashlib
import httplib
import os
import random
import re
import sys
import time
import urllib2 as urllib

from twisted.words.protocols import irc
from twisted.internet import defer
from twisted.internet import protocol
from twisted.internet import task
from twisted.internet import reactor

# Other third-party libraries
import lodgeitlib

# Custom libraries
from hexlib.channel import Channel
from hexlib import authenticator
from hexlib import event
from hexlib import htmlhelper
from hexlib import plugins
from hexlib import stats

# Globals! Should be moved to config files. Done; should be moved to class variables.
ident = True						# Whether to try and identify for Hex_'s nickserv
passwordFile = "password.txt"		# The text file containing the password.
writeLog = True						# Whether we write channel logs or not
email = "rosser.bjr@gmail.com"		# Nickserv registration email

otherChan = ['#gscc', '#jhusps']	# Channel lists, used by bot software
noCmdChan = ['#gscc-minecraft']		# Channels in which no commands are allowed

class IRCBot(irc.IRCClient):

	def __init__(self, *args, **kwargs):
		self._namescallback = {}
		
		self.channels = {}
		self.events = []

	def _get_settings(self):
		return self.factory.settings
	
	def _get_nickname(self):
		return self.factory.nickname
	
	nickname = property(_get_nickname)
	settings = property(_get_settings)

	# Configuration, initial setup that belongs in ^ but doesn't work there for twisted reasons.
	def _configure(self):
		# Do other initial setup
		# Including using those stupid global parameters I haven't gotten rid of yet.
		global otherChan, noCmdChan, writeLog, ident, passwordFile, email
		self.logLocation = "/srv/irc/gscc/"
		self.commandChar = "!"
		if self.settings != None:
			otherChan = self.settings.otherChannels
			noCmdChan = self.settings.noCommandChannels
			writeLog = self.settings.logging
			email = self.settings.nickservEmail
			ident = self.settings.useNickserv
			passwordFile = self.settings.password
		
			self.logLocation = self.settings.logdir
			self.commandChar = self.settings.commandChar
		self.plugins = self.factory.plugins

	def signedOn(self):
		# Run configuration routine here, as we log on.
		self._configure()
		
		global passwordFile, otherChan, ident
		if ident:
			fullPath = passwordFile
			ircWord = authenticator.getPassword(fullPath)
			self.msg('NickServ', 'identify ' + self.nickname + ' ' + ircWord)
		self.join(self.factory.channel)
		for chan in otherChan:
			self.join(chan)
		
	def joined(self, channel):
		self.logPrint(channel, ("%s has joined %s" % (self.nickname, channel)))
		print "*** Joined " + channel
		channelObj = Channel(channel)
		self.channels.update({channel:channelObj})

		# Use NAMES to populate channel user and operator list
		self.names(channel).addCallback(self.gotNames)

	def kickedFrom(self, channel, kicker, message):
		self.logPrint(channel, ("%s was kicked by %s [%s]" % (self.nickname, kicker, message)))
		self.join(channel)
		try:
			self.channels.update({channel:None})
		except IndexError:
			pass
		
	def privmsg(self, user, channel, msg):
		global noCmdChan
		
		#First, if this is a message from a freenode server, parse, print, and return
		if user == None:
			print msg
			return

		#Get rid of the user's hostmask for logging and all other purposes
		shortuser = user[:user.find('!')]
		if channel != self.nickname:
			self.logPrint(channel, "<" + shortuser + "> " + msg)
			userObj = self.channels[channel].getUser(shortuser)
			if userObj == None:
				userObj = self.channels[channel].addUser(shortuser)
				userObj.numMessages += 1
			else:
				userObj.numMessages += 1
		else:
			#Dummy channel created for private messaging conversations
			channel = shortuser
			channelObj = Channel(channel)
			self.channels.update({channel:channelObj})
			self.logPrint(channel, "<" + shortuser + "> " + msg)

		#Channels where commands is allowed
		if channel in noCmdChan:
			return
			
		# Run commands in plugins.
		# shortuser is kind of an essential parameter, so pass that too.
		# Unless essential to the working of the bot, please put all commands
		# in a plugin. (Essential: logging, help, about, join, etc).
		plugins.privmsg(self.plugins, self, user, channel, msg, shortuser)
			
		#Bot commands (use a "!")
		# General syntax for commands
		#  elif x in msg: 				For a command that takes an argument
		#  elif x == msg[1:]:			For a command that doesn't take an argument
		#  elif () and self.channels[channel].isOperator(shortuser): 	Requires op
		#  elif () and channel != shortuser:	Requires a real channel, not privmsg
		#  elif () and ():				Command aliases- two command to do one thing
		if msg[0] == self.commandChar:
			
			#About command, to get Python interpreter and system information
			if "about" == msg[1:] or "source" == msg[1:]:
				version = sys.version
				if "\n" in version:
					version = version.replace("\n", " ")
					self.logSay(channel, "Hex IRC Bot ( https://bitbucket.org/TC01/hex/ ), Running Python " + version)
				else:
					self.logSay(channel, "Hex IRC Bot ( https://bitbucket.org/TC01/hex/ ), Running Python " + sys.version)
			
			#Join command, to join a new channel
			elif "join " in msg and self.channels[channel].hasOperator(shortuser):
				try:
					newChannel = msg[6:]
					self.join(newChannel)
				except:
					self.logSay(channel, "An error has occured!")

			#Logs commands that use the LodgeIt! pastebin
			elif "logs" in msg and self.channels[channel].hasOperator(shortuser):
				if msg[1:] == "logs today":
						dateString = str(datetime.date.today())
				else:
					try:
						dateString = msg[6:]
					except:
						self.logSay(channel, "Invalid use of !logs command")
				channelName = channel[1:]
				if not '#' in channel:
					channelName = self.nickname
				pathRoot = os.path.join(self.logLocation, "logs", channelName, '')
				logPath = pathRoot + dateString + ".log"
				try:
					logFile = open(logPath, 'rt')
				except:
					self.logSay(channel, "Invalid date formatting- use [year]-[month]-[day]")
					return
				pastebin = lodgeitlib.Lodgeit("http://bpaste.net/")
				pasteID = pastebin.new_paste(unicode(logFile.read()), None, private=True)
				pasteURL = pastebin.get_paste_by_id(pasteID).url
				self.msg(shortuser, "Logs from " + dateString + ": " + str(pasteURL))
				logFile.close()
			
			#Nick command, to change bot nickname
			elif "nick" in msg and self.channels[channel].hasOperator(shortuser):
				try:
					newNick = msg[6:]
					self.setNick(newNick)
				except:
					self.logSay(channel, "Error: invalid nickname specified!")
			
			#Quit command, to quit the bot
			elif "quit" == msg[1:] and self.channels[channel].hasOperator(shortuser):
				self.factory.quitting = True
				self.quit("Bot exiting")
			
			# Reload command, to reload the bot
			# There isn't really a good reason to reload the bot right now...
			elif "reload" == msg[1:] and self.channels[channel].hasOperator(shortuser):
				self.factory.quitting = False
				self.quit("Bot reloading")
			
			#Regiter command, to register bot nickname with nickserv
			elif "register" == msg[1:] and self.channels[channel].hasOperator(shortuser):
				global email, opWord
				self.logSay(channel, "Registered nick with nickserv (unless an error occured)")
				self.logSay(channel, "Please run !verify [string] from the verification e-mail")
				ircword = hashlib.sha1(opWord).hexdigest()[:8]
				self.msg('NickServ', 'REGISTER ' + ircword + " " + email)
				
			#Verify command, to verify nickserv registration
			elif "verify" in msg and self.channels[channel].hasOperator(shortuser):
				try:
					verifyCode = msg[8:]
					if verifyCode != "":
						self.msg('NickServ', 'VERIFY REGISTER ' + self.nickname + ' ' + verifyCode)
						self.logSay(channel, "Verified registration with nickserv")					
				except:
					self.logSay(channel, "An error has occured!")
					return
				
			# Schedule command, basic non-plugin implementation of scheduling.
			elif "schedule" in msg[1:]:
				scheduleCmd = msg[msg.find("schedule") + len("schedule") + 1:]
				datestamp, b, scheduled = scheduleCmd.partition(" ")
				try:
					stamp = datetime.datetime.strptime(datestamp, "%H:%M")
					today = datetime.datetime.today()
					stamp = stamp.replace(today.year, today.month, today.day)
					hexEvent = event.HexEvent(self, channel, stamp, scheduled)
					#self.scheduleEvent(hexEvent, channel)
				except ValueError:
					self.logSay(channel, "Error: improper date string.")


		#If the 'channel' is a dummy private message conversation, nuke it
		if channel == shortuser:
			self.channels.update({channel:None})
	
	# External commands and messages!
	def externalMessage(self, message):
		
		# For now, don't do anything by default, but do allow plugins here.
		plugins.externalMessage(self.plugins, self, message)
	
	#Logging functions
	def userJoined(self, user, channel):
		self.logPrint(channel, ("%s has joined %s" % (user, channel)))
		channelObj = self.channels[channel]
		channelObj.addUser(user)
	
	def userLeft(self, user, channel):
		channelObj = self.channels[channel]
		channelObj.removeUser(user)
		self.logPrint(channel, ("%s has left %s" % (user, channel)))
		
	def userQuit(self, user, quitMessage):
		for channelName, channel in self.channels.iteritems():
			if channel.hasUser(user):
				channel.removeUser(user)
				self.logPrint(channelName, ("%s: Quit [%s]" % (user, quitMessage)))
		
	def userKicked(self, kickee, channel, kicker, message):
		channelObj = self.channels[channel]
		channelObj.removeUser(kickee)
		self.logPrint(channel, ("%s was kicked by %s [%s]" % (kickee, kicker, message)))
		
	def topicUpdated(self, user, channel, newTopic):
		self.logPrint(channel, ("%s changes topic to '%s'" % (user, newTopic)))
		
	def action(self, user, channel, data):
		shortlen = user.find('!')
		shortuser = user[:shortlen]
		if shortlen == -1:
			shortuser = user
		self.logPrint(channel, ("* %s %s" % (shortuser, data)))
		
	def userRenamed(self, oldname, newname):
		for channelName, channel in self.channels.iteritems():
			if channel.hasUser(oldname):
				userObj = channel.getUser(oldname)
				userObj.rename(newname)
				self.logPrint(channelName, ("%s is now known as %s" % (oldname, newname)))
				if channel.hasOperator(oldname):
					channel.removeOperator(olname)
					channel.addOperator(newname)
	
	def modeChanged(self, user, channel, set, modes, args):
		#Get the + or - string, then the user's short
		stringSet = '-'
		if set:
			stringSet = '+'
		shortlen = user.find('!')
		shortuser = user[:user.find('!')]
		if shortlen == -1:
			shortuser = user
		
		#Output log information		
		outputMessage = shortuser + " sets mode " + stringSet + modes
		setUser = args[0]
		try:
			outputMessage += " "
			outputMessage += setUser
		except:
			pass
		self.logPrint(channel, outputMessage)
		
		#If this is operator mode, modify the operator statuses in Channel objects
		if 'o' in modes and setUser != "":
			channelObj = self.channels[channel]
			if channelObj.hasUser(setUser):
				if set:
					channelObj.addOperator(setUser)
				else:
					channelObj.removeOperator(setUser)
			elif set:
				userObj = channelObj.addUser(setUser)
				channelObj.addOperator(setUser)
		
	# Miscellaneous functions
	
	def scheduleEvent(self, hexEvent, channel):
		"""Schedules a hex event to run at some point in the future, in the given channel."""
		try:
			reactor.callLater(hexEvent.getStartTime(), hexEvent.run)
		except AssertionError:
			self.logSay(channel, "Error: you tried to schedule an event in the past!")

	def logSay(self, channel, msg):
		"""Both log and say a message from the bot"""
		self.msg(channel, msg)
		self.logPrint(channel, "<" + self.nickname + "> "+ msg)
		
	def logSaySimple(self, msg):
		self.msg(self.channels[0], msg)
		self.logPrint(self.channels[0], "<" + self.nickname + "> " + msg)
		
	def logPrint(self, channel, message):
		"""Function to print activity to the command line and then log it to a file on my system."""
		global writeLog
		
		#Get the name of the log to use
		if channel[0] != '#':
			channel = '#' + channel
		if not channel in self.channels:
			if channel != self.nickname:
				channel = '#'+self.nickname
		
		# Don't log if we're in a channel where we aren't supposed to do that.
		if channel in self.settings.noLoggingChannels:
			return
		
		#Get the local time and channel name
		now = time.localtime()
		hour = fixTime(str(now[3]))
		minute = fixTime(str(now[4]))
		second = fixTime(str(now[5]))
		fullmsg = '[' + fixTime(hour) + ':' + fixTime(minute) + ':' + fixTime(second) + '] ' + message
		chanName = channel[1:]

		# Write the data to the log file
		if writeLog:
			date = str(datetime.date.today())
			logName = date + '.log'
			logFolder = os.path.join(self.logLocation, "logs", chanName, '')
			if not os.path.exists(logFolder):
				try:
					os.makedirs(logFolder)
				except:
					print "Error: unable to set up log folder. This might be a permissions problem."
			try:
				logFile = logFolder + logName
				logs = open(logFile, 'a')
				logs.write(fullmsg + '\n')
				logs.close()
			except:
				print "Error: Unable to write to log file " + logName

	# Implementation of NAMES from stackoverflow.
	# Note: The code I borrowed uses an advanced callback system
	# I do not speak twisted defer, so I have hacked it a bit
	# callbacks must be of the form gotNames(args), args = (channel, users)

	def names(self, channel):
		"""Names function, borrowed from StackOverflow."""
		channel = channel.lower()
		d = defer.Deferred()
		if channel not in self._namescallback:
			self._namescallback[channel] = ([], [])

		self._namescallback[channel][0].append(d)
		self.sendLine("NAMES %s" % channel)
		return d
	
	def irc_RPL_NAMREPLY(self, prefix, params):
		channel = params[2].lower()
		nicklist = params[3].split(' ')

		if channel not in self._namescallback:
			return

		n = self._namescallback[channel][1]
		n += nicklist

	def irc_RPL_ENDOFNAMES(self, prefix, params):
		channel = params[1].lower()
		if channel not in self._namescallback:
			return

		callbacks, namelist = self._namescallback[channel]

		for cb in callbacks:
			args = (channel, namelist)
			cb.callback(args)

		del self._namescallback[channel]

	def gotNames(self, args):
		"""Names callback, takes channel name and userlist. Updates channel object."""
		# Unpack argument tuple that the callback thing passed us
		channel = args[0]
		users = args[1]
		
		# Try to grab the channel object from the channels array.
		try:
			channelObj = self.channels[channel]
		except:
			print "Error: I don't even know how this happened."
			print "Names called by joined for a channel we are no longer in."
			return
			
		# Now, step through the names list.
		for user in users:
			# I think @ and + are the only relevant modes.
			if "@" == user[0]:
				channelObj.addUser(user[1:])
				channelObj.addOperator(user[1:])
			else:
				if "+" == user[0]:
					user = user[1:]
				channelObj.addUser(user)
		self.channels.update({channel:channelObj})

def fixTime(string):
	"""Utility to fix problems for a 'time' string."""
	if len(string) == 1:
		string = '0' + string
	return string
	
# The 'control server' that actually starts the IRC bot.
# Implemented this way so external messages can be passed to the bot!

from twisted.internet import protocol
from twisted.internet import reactor
from twisted.protocols import basic

from hexlib import config

class ControlServer(basic.LineReceiver):
		
	# When we receive a line, pass it to the IRC client.
	def lineReceived(self, line):
		
		# This is lazy but works I guess.
		ircFactory = self.factory.ircFactory
		if ircFactory.connectedProtocol is None:
			print "*** Error: we received some data, but the IRC client wasn't connected."
			print "*** If TC01 were a real networking person he'd have fixed this by now. But the data was: "
			print line

		# XXX: maybe do some processing here first?
		ircFactory.connectedProtocol.externalMessage(line)